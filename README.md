
Uso del Plugin Palette_Generator para crear un carrusel de imagenes
que cambian de color de fondo segun la paleta de colores de la imagen
que se muestra.
Serviría como para cuando quieres mostrar un producto y que el color 
del fondo que lo acompaña cambie al color predominante de la imagen del producto.

Para que el texto no haga "Overflow" y salte ese error de la "banda 
amarilla y negra" se hizo uso del widget "SingleChildScrollView"
del cual tenemos un ejemplo ya hecho aquí:

De aquí el video tutorial:
https://www.youtube.com/watch?v=UGG6LLVB0BY

pero le hice una mejora porque la forma de como mapear la lista de
imagenes que se ve en el video no funciona en Flutter 1.9

Así fue como quedó:
![Pantalla Inicial](assets/Screenshot_2019-11-25-15-41-06.png)
![](assets/Screenshot_2019-11-25-15-41-11.png)
![](assets/Screenshot_2019-11-25-15-41-14.png)
![](assets/Screenshot_2019-11-25-15-41-17.png)


NOTA: OJO CON EL NOMBRE DEL DIRECTORIO. NO CONFUNDIR CON EL NOMBRE
DEL PLUGGIN, No es "pallete_generator_app" es "palette_generator_app"
me equivoque al escribir el nombre..!

# pallete_generator_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
