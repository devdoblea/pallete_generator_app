import 'package:flutter/material.dart';
import 'package:palette_generator/palette_generator.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Palette Generator',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Exo2',
      ),
      home: HomePage()
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  final List<String> _imagenes = [
    'assets/images/eiffel-tower-1156146_960_720.jpg',
    'assets/images/gyrfalcon-2678684_960_720.jpg',
    'assets/images/mountains-1828596_960_720.jpg',
    'assets/images/plouzane-1758197_960_720.jpg',
  ];

  List<PaletteColor> colores;
  int _currentIndex;

  _updatePalettes() async {
    for (var imagen in _imagenes) {
      final PaletteGenerator generador = await
        PaletteGenerator.fromImageProvider(
          AssetImage(imagen),
          size: Size(200.0 ,100.0)
        );
      colores.add(
        generador.lightMutedColor != null
        ? generador.lightMutedColor
        : PaletteColor(Colors.blue,2)
      );
    }
    setState(() {
      //
    });
  }

  @override
  void initState() { 
    super.initState();
    colores = [];
    _currentIndex = 0;
    _updatePalettes();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Torre Eiffel'),
        elevation: 0,
        backgroundColor: colores.isNotEmpty
        ? colores[_currentIndex].color
        : Theme.of(context).primaryColor,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget> [
          Container(
            width: double.infinity,
            height: 200,
            // aqui cambia el backgroundColor segun la paleta de colores
            color: colores.isNotEmpty 
                   ? colores[_currentIndex].color
                   : Colors.white,
            // aqui genera la pagina con los datos de las imagenes
            child: PageView.builder(
              onPageChanged: (index) {
                setState(() {
                  _currentIndex= index;
                });
              },
              itemCount: _imagenes.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  padding: const EdgeInsets.all(16.0),
                  margin: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30.0),
                    image: DecorationImage(
                      // con esto Iteran las imagenes
                      image: AssetImage(_imagenes[index]),
                      fit: BoxFit.cover
                    )
                  ),
                );
              },
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(22.0),
              width: double.infinity,
              decoration: BoxDecoration(
                color: colores.isNotEmpty
                       ? colores[_currentIndex].color
                       : Colors.black,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Torre Eiffel, la Septima Maravilla",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 30.0,
                      ),
                    ),
                    // Para dividir con un espacio en blanco de un ancho especifico
                    const SizedBox(height: 10.0),
                    Text(
                      """Consectetur ex officia non qui nostrud pariatur cupidatat ut. Cillum consequat cillum voluptate cupidatat pariatur quis aliquip consequat nostrud mollit anim non eu officia. Ullamco fugiat dolore tempor sin. Ex sunt proident aute in proident ipsum aute dolor cillum enim adipisicing. Mollit ut consequat reprehenderit occaecat non minim veniam pariatur minim amet ut ipsum laborum. Commodo cupidatat aute pariatur nostrud esse. Labore non esse aliqua velit laboris irure est dolore dolor. Aliqua magna minim in excepteur est labore cupidatat dolore. Consequat cupidatat aliqua incididunt sit aute esse nisi nostrud proident cupidatat do incididunt anim.""",
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}